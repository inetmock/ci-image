FROM ubuntu:20.04

ARG MDBOOK_VERSION=0.4.7

RUN apt update && \
    apt install -y wget && \
    apt clean all -y && \
    wget -qO- "https://github.com/rust-lang/mdBook/releases/download/v${MDBOOK_VERSION}/mdbook-v${MDBOOK_VERSION}-x86_64-unknown-linux-gnu.tar.gz" | tar -xz -C /usr/bin/