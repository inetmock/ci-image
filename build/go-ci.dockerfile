FROM docker.io/golang:1.18-alpine

ARG GITHUB_TOKEN

# install basic dependencies
RUN apk add -U --no-cache \
        docker \
        protobuf-dev \
        findutils \
        git \
        git-lfs \
        ca-certificates \
        graphviz \
        gcc \
        musl-dev \
        curl \
        jq \
        libbpf \ 
        libbpf-dev \
        clang \
        llvm12 \
        elfutils \
        linux-headers

# Install kubernetes tools
RUN curl -Lo /usr/local/bin/kubectl "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x /usr/local/bin/kubectl && \
    export HELM_VERSION=$(curl https://api.github.com/repos/helm/helm/releases | jq -r '. | first |.tag_name') && \
    curl -L "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" | tar -xvz --strip-components=1 -C /usr/local/bin linux-amd64/helm && \
    kubectl version --client=true && \
    helm version

# Install GoReleaser
RUN export GORELEASER_VERSION=$(curl https://api.github.com/repos/goreleaser/goreleaser/releases | jq -r '. | first |.tag_name | capture("(?<version>[[:digit:]]+\\.[[:digit:]]+\\.[[:digit:]]+)") | .version') && \
    curl -Lo /tmp/goreleaser.apk "https://github.com/goreleaser/goreleaser/releases/latest/download/goreleaser_${GORELEASER_VERSION}_x86_64.apk" && \
    apk add --allow-untrusted /tmp/goreleaser.apk && \
    rm -f /tmp/*.apk && \
    goreleaser --version

# Install go tools
RUN go install github.com/golang/mock/mockgen@latest \
    && go install google.golang.org/protobuf/cmd/protoc-gen-go@latest \
    && go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest \
	&& go install github.com/t-yuki/gocover-cobertura@latest \
    && go install github.com/Helcaraxan/gomod@latest \
    && go install gotest.tools/gotestsum@latest \
    && go install golang.org/x/tools/cmd/goimports@latest \
    && go install mvdan.cc/gofumpt@latest \
    && go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest \
    && curl -L https://github.com/go-task/task/releases/latest/download/task_linux_amd64.tar.gz | tar -xvz -C /usr/local/bin task \
	&& export BUF_VERSION=$(curl https://api.github.com/repos/bufbuild/buf/releases | jq -r '. | first |.tag_name') && \
    curl -Lo /usr/bin/buf "https://github.com/bufbuild/buf/releases/download/${BUF_VERSION}/buf-Linux-x86_64" \
	&& chmod +x /usr/bin/buf \
    && go version \
    && golangci-lint --version \
    && task --version \
    && buf --version
